# Wazuh_alerts_parser

A parser that extracts some information about Wazuh Manager alerts.log as well as count the number of alerts generated in a date given the --count YYYY/MMM/DD parameter.