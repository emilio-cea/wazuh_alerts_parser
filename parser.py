'''
This program parses the last alert if no arguments given, following the format of the Wazuh application.
By forwarding the '--count YYYY/MM/DD' argument this program counts the alerts generated in that date.
'''
import re
import os.path
import sys

# variable declaration
timestamp_match = 0
id_match = 0
level_match = 0
log_match = '0'
my_file = []
count = 0

# get the path to the alert file, which is in the current working directory
log_file_path = os.path.join(sys.path[0], "alerts.log")
log_file = open(log_file_path, "r")

# read the file
lines = log_file.readlines()

# we check the number of arguments passed to our program, if it's only 1 we are checking the last alert
if len(sys.argv) == 1:
    # we read from the end of the file backwards and finish reading when we encounter the string '**' indicating an
    # alert
    for line in reversed(lines):
        # print(line)
        my_file.append(line)
        if line.startswith('**'):
            break
    my_file.reverse()

    # in this regex, group 1 is the timestamp and group 2 the log
    regex_timestamp_log = '(\d{4} \w{3} \d{1,2} \d{1,2}:\d{1,2}:\d{1,2}) \w+->(.+)'

    # this regex captures the rule id on the first group and the alert level on the second
    regex_id_level = 'Rule: (\d+) \(level (\d+)\)'

    # we need to compile de expressions
    event_timestamp_log = re.compile(regex_timestamp_log)
    id_level = re.compile(regex_id_level)

    # we iterate through the alert line by line matching our regex expressions
    for i, line in enumerate(my_file):
        for match in re.finditer(event_timestamp_log, line):
            # print(f'Found on line {i + 1}: {match.group()}')
            timestamp_match = match.group(1)
            log_match = match.group(2)
        for match in re.finditer(id_level, line):
            # print('Found on line %s: %s' % (i + 1, match.group()))
            id_match = match.group(1)
            level_match = match.group(2)

    print('Last alert information:')
    print('- Timestamp: ', timestamp_match)
    print('- Rule ID: ', id_match)
    print('- Rule level: ', level_match)
    print('- Log file: ', log_match)

# we check if the next argument passed is --count and there is another argument por the date
elif sys.argv[1] == '--count' and len(sys.argv) == 3:
    date = str(sys.argv[2])

    # we strip the '/' character ti fit the format on the logs 2020/Jul/23 --> 2020 Jul 23
    date_clean = date.replace('/', ' ')

    # we are going to check if the parameter passed as date is a correct date format, if not we exit the program
    new_date = ''
    regex_check_date = '(\d{4}\/\w{3}\/\d{1,2})\\b'
    check_date = re.compile(regex_check_date)

    for match in re.finditer(check_date, date):
        new_date = match.group()

    if new_date == '':
        print('Please enter date in the correct format YYYY/MM/DD')
        exit(0)

    # if the format is correct we read the file line by line and check only por the date on the timestamp which we
    # can always find after the first line starting with '**'
    for i, line in enumerate(lines):
        if i > 0:
            previous = lines[i - 1]
            if line.__contains__(date_clean) and previous.startswith('**'):
                count = count + 1

    print(f'Alerts generated in {date}: {count}')

# in case something is not correct we exit the program
else:
    print("No valid input, please do it again")
    exit(0)

log_file.close()
